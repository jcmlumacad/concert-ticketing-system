<?php
session_start();
$headers = ['assets/css/dashboard.css'];
$active = 'reports';
?>
<?php include_once('./../layouts/header.php'); ?>
<?php include_once('./../layouts/nav.php'); ?>
<div class="container-fluid">
  <div class="row">
    <?php include_once('sidebar.php'); ?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <?php include_once('./../views/dashboard.php'); ?>
    </div>
  </div>
</div>
<?php include_once('./../layouts/footer.php'); ?>

