<?php

$connection = require_once './core/Connection.php';

class Login
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function authenticate($params)
    {
        $query = $this->pdo->prepare("
            SELECT profile.*, username, roles.name as role_name FROM users 
            INNER JOIN roles ON roles.id = users.role_id
            INNER JOIN profile ON profile.user_id = users.id
            WHERE BINARY users.username='" . $params['username'] . "' && users.password='" . md5($params['password']) . "'
            LIMIT 1");
        $query->execute();

        if ($query->rowCount()) {
            return $query->fetch(PDO::FETCH_OBJ);
        }

        return false;
    }
}

return new Login($connection);