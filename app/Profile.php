<?php

$connection = require_once './core/Connection.php';

class Profile
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function store($params)
    {
        $role = $this->pdo->prepare("SELECT * FROM roles WHERE name='user' LIMIT 1");
        $role->execute();

        if ($role->rowCount()) {
            $roleId = $role->fetch(PDO::FETCH_OBJ)->id;

            $user = $this->pdo->prepare("
                INSERT INTO users VALUES('', $roleId,'" . $params['username'] . "','" . md5($params['password']) . "')
            ");
            $user->execute();
            $userId = $this->pdo->lastInsertId();

            $profile = $this->pdo->prepare("
                INSERT INTO profile VALUES('', $userId, '" . $params['name'] . "', '" . $params['age'] . "', '" . $params['address'] . "', '" . $params['contact_number'] . "')
            ");
            $profile->execute();
            $profileId = $this->pdo->lastInsertId();

            return true;
        }

        return false;
    }
}

return new Profile($connection);