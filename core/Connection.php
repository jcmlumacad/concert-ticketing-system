<?php

class Connection
{
    public static function make($config)
    {
        try {
            return new PDO(
                $config['driver'] . ':host=' . $config['host'] . ';dbname='.$config['name'],
                $config['username'],
                $config['password'],
                $config['options']);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}

$connection = new Connection();
return $connection->make([
    'name'      => 'ticketing',
    'username'  => 'root',
    'password'  => '',
    'driver'    => 'mysql',
    'host'      => '127.0.0.1',
    'options'   => [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
    ]
]);