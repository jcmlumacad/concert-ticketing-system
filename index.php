<?php
session_start();
if (count($_POST)) {

    $login = require './app/Login.php';
    $user = $login->authenticate($_POST);

    if ($user) {
        $_SESSION['isLoggedIn'] = true;
        foreach ($user as $key => $value) {
            $_SESSION[$key] = $value;
        }

        if ($user->role_name === 'Administrator') {
            header('Location:admin/dashboard.php');
            exit(0);
        }

        header('Location:public/dashboard.php');
        exit(0);
    }

    $errorMessage = 'Invalid credentials';
}
?>
<?php $headers = ['assets/css/login.css']; ?>
<?php include_once('layouts/header.php'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 main">
      <?php include_once('views/login.php'); ?>
    </div>
  </div>
</div>
<?php include_once('layouts/footer.php'); ?>