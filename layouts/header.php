<?php
$base = explode('/', $_SERVER['SCRIPT_NAME']);
$basePath = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/' . $base[1] . '/';

if ( ! isset($_SESSION['isLoggedIn']) && ($base[2] === 'admin' || $base[2] === 'public')) {
    header('Location:' . $basePath . 'index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Online Concert Ticketing System</title>
  <link rel="icon" href="http://getbootstrap.com/favicon.ico">
  <link rel="stylesheet" type="text/css" href="<?php echo $basePath . 'assets/css/bootstrap.min.css'; ?>">
  <?php
    if (isset($headers)) {
        foreach ($headers as $header) {
            echo "<link rel='stylesheet' type='text/css' href='" . $basePath . $header . "'>";
        } 
    }
  ?>
</head>
<body>