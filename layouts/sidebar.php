<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <li <?= _linkActive($active, 'profile') ?>><a href="<?php echo $basePath . 'public/profile.php'; ?>">Profile</a></li>
    <li <?= _linkActive($active, 'events') ?>><a href="<?php echo $basePath . 'public/events.php'; ?>">Events</a></li>
  </ul>
</div>

<?php
function _linkActive($tab, $name)
{
    return isset($tab) && $tab === $name ? 'class="active"' : '';
}
?>