<?php
session_start();

if (count($_POST)) {

    $profile = require './app/Profile.php';
    $user = $profile->store($_POST);

    if ($user) {
        $_SESSION['isLoggedIn'] = true;
        // foreach ($user as $key => $value) {
        //     $_SESSION[$key] = $value;
        // }
        header('Location:public/dashboard.php');
        exit(0);
    }

    $errorMessage = 'Register failed';
}
?>
<?php $headers = ['assets/css/login.css']; ?>
<?php include_once('layouts/header.php'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 main">
      <?php include_once('views/register.php'); ?>
    </div>
  </div>
</div>
<?php include_once('layouts/footer.php'); ?>