<div class="container" style="padding-top: 100px;">
  <div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
      <div class="account-wall">
        <img class="profile-img" src="<?php echo $basePath . 'assets/img/photo.png'; ?>">
        <form class="form-signin" method="POST" action="<?php echo $basePath . 'index.php'; ?>">
          <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
          <input type="password" class="form-control" placeholder="Password" name="password" required>
          <button class="btn btn-primary btn-block" type="submit">Login</button>
        </form>
      </div>
      <a href="<?= $basePath . 'register.php' ?>" class="text-center new-account">Create new profile</a>
    </div>
  </div>
</div>