<div class="container" style="padding-top: 100px;">
  <div class="row">
    <div class="col-sm-5 col-md-4 col-md-offset-4">
      <div class="well">
        <h3 class="text-center">Registration</h3>
        <form class="form form-horizontal" method="POST" action="<?php echo $basePath . 'register.php'; ?>" style="padding: 30px;">
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" placeholder="Password" name="password" required>
          </div>
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" placeholder="Name" name="name" required>
          </div>
          <div class="form-group">
            <label>Age</label>
            <input type="number" class="form-control" placeholder="Age" name="age" min="1" max="100" required>
          </div>
          <div class="form-group">
            <label>Address</label>
            <textarea class="form-control" placeholder="Address" name="address" required></textarea>
          </div>
          <div class="form-group">
            <label>Contact Number</label>
            <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" required>
          </div>
          <button class="btn btn-success btn-block" type="submit">Register</button>
        </form>
      </div>
    </div>
  </div>
</div>